'use strict';

/**
 * attachments-section controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::attachments-section.attachments-section');
