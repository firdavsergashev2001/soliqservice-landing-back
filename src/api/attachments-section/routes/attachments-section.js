'use strict';

/**
 * attachments-section router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::attachments-section.attachments-section');
