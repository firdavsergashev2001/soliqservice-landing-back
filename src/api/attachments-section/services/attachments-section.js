'use strict';

/**
 * attachments-section service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::attachments-section.attachments-section');
