'use strict';

/**
 * contact-info-and-app-link controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::contact-info-and-app-link.contact-info-and-app-link');
