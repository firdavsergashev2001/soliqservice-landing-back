'use strict';

/**
 * contact-info-and-app-link router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::contact-info-and-app-link.contact-info-and-app-link');
