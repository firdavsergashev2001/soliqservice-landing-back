'use strict';

/**
 * contact-info-and-app-link service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::contact-info-and-app-link.contact-info-and-app-link');
